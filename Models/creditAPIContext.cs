﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace creditAPI.Models
{
    public partial class creditAPIContext : DbContext
    {
        public creditAPIContext()
        {
        }

        public creditAPIContext(DbContextOptions<creditAPIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TM_Bank> TM_Bank { get; set; }
        public virtual DbSet<T_Users> T_Users { get; set; }
        public virtual DbSet<employees> employees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Name=DefaultConnect");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<TM_Bank>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.dCreate).HasColumnType("datetime");

                entity.Property(e => e.dUpdate).HasColumnType("datetime");

                entity.Property(e => e.sCode)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.sDescription)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.sName)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<T_Users>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.dCreate).HasColumnType("datetime");

                entity.Property(e => e.dUpdate).HasColumnType("datetime");

                entity.Property(e => e.sPassword)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.sUsername)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<employees>(entity =>
            {
                entity.Property(e => e.id).ValueGeneratedNever();

                entity.Property(e => e.name).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
