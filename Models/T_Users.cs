﻿using System;
using System.Collections.Generic;

#nullable disable

namespace creditAPI.Models
{
    public partial class T_Users
    {
        public int id { get; set; }
        public string sUsername { get; set; }
        public string sPassword { get; set; }
        public int? age { get; set; }
        public DateTime? dCreate { get; set; }
        public int? nCreate { get; set; }
        public DateTime? dUpdate { get; set; }
        public int? snUpdate { get; set; }
    }
}
