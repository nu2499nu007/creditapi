﻿using System;
using System.Collections.Generic;

#nullable disable

namespace creditAPI.Models
{
    public partial class TM_Bank
    {
        public int id { get; set; }
        public string sName { get; set; }
        public DateTime? dCreate { get; set; }
        public int? nCreate { get; set; }
        public DateTime? dUpdate { get; set; }
        public int? nUpdate { get; set; }
        public string sDescription { get; set; }
        public string sCode { get; set; }
    }
}
