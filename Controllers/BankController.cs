using System;
using System.Collections.Generic;
using creditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
namespace creditapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BankController : ControllerBase
    {
        private readonly ILogger<BankController> _logger;
        private readonly creditAPIContext _db;
        public BankController(ILogger<BankController> logger, creditAPIContext db)
        {
            _logger = logger;
            _db = db;
        }
        [HttpGet("GetBankALL")]
        public ActionResult<IEnumerable<TM_Bank>> Get()
        {
            try
            {
                return _db.TM_Bank;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}