using System;
using System.Collections.Generic;
using creditAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
namespace creditapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly creditAPIContext _demoContext;
        public EmployeeController(ILogger<EmployeeController> logger, creditAPIContext demoContext)
        {
            _logger = logger;
            _demoContext = demoContext;
        }
        [HttpGet("GetEmployeesData")]
        public ActionResult<IEnumerable<employees>> Get()
        {
            try
            {
                return _demoContext.employees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
